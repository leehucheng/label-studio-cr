#!/bin/bash
# example usage:
# $bash download_cr.sh PeePoo \
#                      /KIKI/hucheng/cr_video \

python download_CR.py --bucket 'ai-furbo-test-data-collection' \
                      --start_date '2021-08-23' \
                      --end_date '2021-08-26' \
                      --device 'Furbo' \
                      --cls_name $1 \
                      --target_dir $2 \
                      --sample_num 10