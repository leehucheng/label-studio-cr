#!/bin/bash
# $1: the directory of target files
# $2: the filepath of json file
# $3: the directory of your labeled results
# $4: the port of label studio
# example usage:
# $bash cr-label-local.sh /KIKI/hucheng/cr_video/Furbo/2021-08-23/PeePoo \
#                   task_20210823_peepoo.json \
#                   /KIKI/hucheng/cr_video_result/20210823_peepoo \
#                   8787

# step1 - find target mp4 files and generate corresponding json file
python generate_json.py --data_dir $1 --json_filename $2 --local True
# step2 - launch http-server and upload files for label studio
#http-server $1 -p $3 --cors &
# step3 - launch label-studio and load mp4 by json format
label-studio init $3 --input-path $2 --input-format json --allow-serving-local-files --label-config config-cr.xml
label-studio start $3 --port $4
