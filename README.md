# Label-Studio CR (v0.9.1)

## prerequisite
install HTTP server and set up CORS for that so that Label Studio can access the data files successfully

```bash
$ npm install http-server -g
$ http-server -p 3000 --cors
```

install requirements.txt and make sure your label studio version is not newer than v1.0.0

```bash
$ pip install -r requirements.txt
```

## Download CR
you can download CR by executing ```download_cr.sh```. Since we use boto3 to list the objects, rememeber to config the AWS credential first.

example usage:

```bash
$ bash download_cr.sh PeePoo \
                     /KIKI/hucheng/cr_video \
```

## Launch Label-Studio Service
1. Launch HTTP-SERVER version
example usage:
```bash
$ bash cr-label.sh /KIKI/hucheng/cr_video/Furbo/
                  2021-08-23/PeePoo \
                  task_20210823_peepoo.json \
                  3000 \
                  172.17.0.127 \
                  /KIKI/hucheng/cr_video_result/20210823_peepoo \
                  8787
```
2. Directly read from local path without launching HTTP-SERVER version
example usage:
```bash
$ bash cr-label-local.sh /KIKI/hucheng/cr_video/Furbo/2021-08-23/PeePoo \
                         task_20210823_peepoo.json \
                         /KIKI/hucheng/cr_video_result/20210823_peepoo \
                         8787
```