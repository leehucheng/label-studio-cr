import os
import glob
from tqdm import tqdm
import json
import os 
import argparse
import urllib.parse

def save_json(json_filename, meta):
    with open(json_filename, 'w') as f:
        json.dump(meta, f)

def main(args):
    videos = sorted(glob.glob(os.path.join(args.data_dir, '*.{}'.format(args.extention))))
    meta = []
    
    for video in tqdm(videos):
        video_basename = os.path.basename(video)
        tmp = {}
        tmp["data"] = {}
        if args.extention == 'mp4':
            if args.local == False:
                tmp["data"]["video"] = "<video src='http://{}:{}/{}'".format(args.ip_address, args.port, video_basename)
                if args.version == 'old':
                    tmp["data"]["video"] += " width=100% muted /><img src onerror=\"$=n=>document.getElementsByTagName(n)[0];a=$('audio');v=$('video');a.onseeked=()=>{v.currentTime=a.currentTime};a.onplay=()=>v.play();a.onpause=()=>v.pause()\" />"
                elif args.version == 'new':
                    tmp["data"]["video"] += " width=100% muted/><img src onerror=\"$=n=>document.querySelector(n);a=$('.ls-editor audio');v=$('video');a.onseeked=()=>{v.currentTime=a.currentTime};a.onplay=()=>v.play();a.onpause=()=>v.pause()\" />"
                tmp["data"]["videoSource"] = "http://{}:{}/{}".format(args.ip_address, args.port, video_basename)
            elif args.local == True:
                if args.version == 'old':
                    filename = os.path.basename(video)
                    image_url_path = urllib.parse.quote('data/' + filename)
                    params = urllib.parse.urlencode({'d': os.path.dirname(video)})
                    tmp["data"]["video"] = "<video src='{}?{}'".format(image_url_path, params)
                    tmp["data"]["video"] += " width=100% muted /><img src onerror=\"$=n=>document.getElementsByTagName(n)[0];a=$('audio');v=$('video');a.onseeked=()=>{v.currentTime=a.currentTime};a.onplay=()=>v.play();a.onpause=()=>v.pause()\" />"
                    tmp["data"]["videoSource"] = "{}?{}".format(image_url_path, params)
                elif args.version == 'new':
                    tmp["data"]["video"] = "<video src='data/local-files?d={}'".format(video)
                    tmp["data"]["video"] += " width=100% muted/><img src onerror=\"$=n=>document.querySelector(n);a=$('.ls-editor audio');v=$('video');a.onseeked=()=>{v.currentTime=a.currentTime};a.onplay=()=>v.play();a.onpause=()=>v.pause()\" />"
                    tmp["data"]["videoSource"] = "data/local-files?d={}".format(video)
        elif args.extention == 'wav':
            if args.local == False:
                tmp["data"]["audio"] = "http://{}:{}/{}".format(args.ip_address, args.port, video_basename)
            elif args.local == True:
                if args.version == 'old':
                    filename = os.path.basename(video)
                    image_url_path = urllib.parse.quote('data/' + filename)
                    params = urllib.parse.urlencode({'d': os.path.dirname(video)})
                    tmp["data"]["audio"] = "{}?{}".format(image_url_path, params)
                elif args.version == 'new':
                    tmp["data"]["audio"] = "data/local-files?d={}".format(video)
        meta.append(tmp)
    
    save_json(args.json_filename, meta)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='generate json file for label studio')
    parser.add_argument('-d', '--data_dir', default='/KIKI/hucheng/cr_video/Furbo/2021-08-23/EatDrink', type=str, help='the directory of cr videos')
    parser.add_argument('-f', '--json_filename', default='task.json', type=str, help='the filename of json file')
    parser.add_argument('-p', '--port', default=3000, type = int, help='the port of local file uploading')
    parser.add_argument('-i', '--ip_address', default='172.17.0.127', type = str, help='the ip address of local file uploading')
    parser.add_argument('-e', '--extention', default='mp4', type= str, choices=['mp4','wav'], help='the extention of files you want to generate')
    parser.add_argument('-v', '--version', default='old', type = str, choices= ['old','new'], help='the version before/after v1.0.0')
    parser.add_argument('-l', '--local', default=False, type = bool, help='Whether to use local prefix')
    args = parser.parse_args()

    main(args)