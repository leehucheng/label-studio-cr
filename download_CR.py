import boto3
import os 
import argparse
import datetime
import csv
from pprint import pprint
from tqdm import tqdm
import random
import subprocess

s3_client = boto3.client('s3')

def download_dir(prefix, local, bucket, cls_name, client=s3_client):
    """
    params:
    - prefix: pattern to match in s3
    - local: local path to folder in which to place files
    - bucket: s3 bucket with target contents
    - client: initialized s3 client object
    """
    keys = []
    dirs = []
    next_token = ''
    base_kwargs = {
        'Bucket':bucket,
        'Prefix':prefix,
    }
    while next_token is not None:
        kwargs = base_kwargs.copy()
        if next_token != '':
            kwargs.update({'ContinuationToken': next_token})
        results = client.list_objects_v2(**kwargs)
        contents = results.get('Contents')
        if contents is not None:
            for i in contents:
                k = i.get('Key')
                if k[-1] != '/':
                    keys.append(k)
                else:
                    dirs.append(k)
            next_token = results.get('NextContinuationToken')
        else:
            break

    print(len(keys))

    if len(keys) > 0:
        date, cls_name, deviceid, filename = keys[0].split('/')
        filename = filename.split('_')[0]
        target_dir = os.path.join(local, args.device)
        if not os.path.exists(target_dir):
            os.mkdir(target_dir)
        target_dir = os.path.join(target_dir, date)
        if not os.path.exists(target_dir):
            os.mkdir(target_dir)
        target_dir = os.path.join(target_dir, cls_name)
        if not os.path.exists(target_dir):
            os.mkdir(target_dir)

        exists = set()
        random.Random(2700).shuffle(keys)
        
        with open(os.path.join(target_dir, '{}_{}.csv'.format(cls_name,date)),'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['bucket','date','class_name','deviceid','filename'])
            cnt = 0
            for key in tqdm(keys):
                if cnt > args.sample_num:
                    break
                date, cls_name, deviceid, filename = key.split('/')
                filename = filename.split('_')[0]
                new_filename = '{}_{}'.format(deviceid, filename)
                # skip if exists
                if new_filename in exists:
                    continue
                # download video
                with open(os.path.join(target_dir,'tmp.txt'), 'w') as f:
                    for part in range(1,4):
                        origin_path = key[:-1]+str(part)
                        final_path = os.path.join(target_dir, new_filename+'.mp4.'+str(part))
                        f.write("file {}\n".format(final_path))
                        if (not os.path.exists(final_path)) and (not os.path.exists(os.path.join(target_dir, new_filename+'.mp4'))):
                            try:
                                client.download_file(bucket, origin_path, final_path)
                            except:
                                pass
                cnt += 1
                exists.add(new_filename)
                writer.writerow([args.bucket, date, cls_name, deviceid, filename])
                # concat video
                cmd = "ffmpeg -safe 0 -f concat -i {}/tmp.txt -c copy {}/{}.mp4".format(target_dir, target_dir, new_filename)
                if not os.path.exists(os.path.join(target_dir, new_filename+'.mp4')):
                    try:
                        subprocess.call(cmd.split())
                    except:
                        pass
                # delete split video
                for part in range(1,4):
                    final_path = os.path.join(target_dir, new_filename+'.mp4.'+str(part))
                    if os.path.exists(final_path):
                        os.remove(final_path)


def main(args):
    # start/end date
    start_date = datetime.datetime.strptime(args.start_date,"%Y-%m-%d")
    end_date = datetime.datetime.strptime(args.end_date, "%Y-%m-%d") if isinstance(args.end_date, str) else args.end_date
    assert start_date <= end_date

    # type of device
    device = ''
    if args.device == 'Furbo':
        device = 'B'
    elif args.device == 'Mini':
        device = 'E'
    bucket = args.bucket
    
    # create dir
    if not os.path.exists(args.target_dir):
        os.mkdir(args.target_dir)
        print('create directory {}'.format(args.target_dir))

    while start_date <= end_date:
        start_str = start_date.strftime("%Y-%m-%d")
        start_date += datetime.timedelta(days=1)
        prefix = '{}/{}/{}'.format(start_str,args.cls_name,device)
        print(prefix)
        download_dir(prefix, args.target_dir, bucket, args.cls_name)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='download files from s3')
    parser.add_argument('-b', '--bucket', default='ai-furbo-test-data-collection', type=str, help='bucket name')
    parser.add_argument('-s', '--start_date', default='2021-08-23', type=str, help='start date of download')
    parser.add_argument('-e', '--end_date', default=datetime.datetime.now(), type = str, help='end date of download')
    parser.add_argument('-d', '--device', default='Furbo', type=str, choices=['Furbo','Mini','all'], help='device type')
    parser.add_argument('-c', '--cls_name', default='EatDrink', choices=['PeePoo','EatDrink','Run','CRNoEvent'], help='class')
    parser.add_argument('-t', '--target_dir', default='/KIKI/hucheng/cr_video', type=str, help='the directory of download files')
    parser.add_argument('-n', '--sample_num', default=100, type=int, help='the sampling numuber of daily CR')
    args = parser.parse_args()

    main(args)
    